# Experimental Rivet Analysis for ALICE 

```
$ make install
```
will build and install `RivetAnalysis.so` with its data to the directory found by `rivet-config`.

You can inspect exemplary results here: <https://apn-pucky.gitlab.io/rivet-ALICE_2018_I1680638/>

## References/Datasets 
- <https://inspirehep.net/literature/1680638> (<https://www.hepdata.net/record/ins1680638>)
