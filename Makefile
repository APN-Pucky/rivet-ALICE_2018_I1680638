SRCS := $(wildcard *.cc)
OBJS := $(patsubst %.cc,Rivet%.so,$(SRCS))
NAMES := $(patsubst %.cc,Rivet%,$(SRCS))

ddir := $(shell rivet-config --datadir)
ldir := $(shell rivet-config --libdir)

all: install

build: $(OBJS)

Rivet%: Rivet%.so
	cp $< $(ldir)/Rivet/
	-cp $(patsubst Rivet%.so,%,$<).plot $(ddir)/
	-cp $(patsubst Rivet%.so,%,$<).info $(ddir)/
	-cp $(patsubst Rivet%.so,%,$<).yoda $(ddir)/

Rivet%.so: %.cc
	rivet-build $@ $(patsubst Rivet%.so,%.cc,$@)
	#rivet-build $@ $(patsubst Rivet%.so,%.cc,$@) -std=c++17

install: $(NAMES)

clean:
	rm *.so
