BEGIN PLOT /ALICE_2018_I1680638/.*
LegendAlign=r
MainPlot=1
RatioPlot=1
END PLOT


BEGIN PLOT /ALICE_2018_I1680638/d01-x01-y01
Title=Dielectron invariant-mass spectrum measured in central Pb-Pb collisions at $\\sqrt{s_{NN}}$ = 2.76 TeV. The statistical and systematic uncertainties of the data are represented by vertical bars and boxes.
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1680638/mass_*
Title=Dilepton invariant mass spectrum
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT